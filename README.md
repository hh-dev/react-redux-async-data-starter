# React + Redux w/ Async Data Starter

## System requirements
- Node.js 10.x
- NPM 5.x || 6.x

## Project setup
- Clone repository: `git clone git@bitbucket.org:hh-dev/react-redux-async-data-starter.git`
- Install dependencies: `npm i`

## Development
In the _package.json_ file, a convenience script, which uses Gulp has been included to start the app. Simply run 
```
npm start
```

The app should start on port 8080
