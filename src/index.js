import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { Route } from 'react-router';
import { ConnectedRouter } from 'connected-react-router'
import store, { history } from './store/index';
import Dashboard from './components/dashboard/index';
import ImagesGrid from './components/gallery/images-grid';
import ImagePreview from './components/gallery/image-preview';
import './theme/scss/bootstrap.scss';
import './theme/scss/app.scss';

const Root = (
    <Provider store={store}>
        <ConnectedRouter history={history}>
            <div className="app">
                <Route exact={true} path="/" component={Dashboard} />
                <Route exact={true} path="/images" component={ImagesGrid} />
                <Route exact={true} path="/images/:imageId" component={ImagePreview} />
            </div>
        </ConnectedRouter>
    </Provider>
);

render(Root, document.getElementById('root'));
