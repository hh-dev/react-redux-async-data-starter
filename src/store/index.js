import { createStore, compose, applyMiddleware } from 'redux';
import createHistory from 'history/createBrowserHistory';
import { connectRouter, routerMiddleware } from 'connected-react-router'
import apiRequestMiddleware from './middleware/api-request';
import rootReducer from './reducers/index';

export const history = createHistory();

const middlewares = applyMiddleware(routerMiddleware(history), apiRequestMiddleware);

const index = createStore(
    connectRouter(history)(rootReducer),
    compose(middlewares)
);

export default index;
