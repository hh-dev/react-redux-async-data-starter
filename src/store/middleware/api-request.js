import _ from 'lodash';
import axios from 'axios';
import querystring from 'querystring';
import getDataInitialState from '../get-data-initial-state';
import { API_REQUEST } from '../actions/types';
import { API_BASE_URL } from '../../settings';

const handleRequest = async (url, method, data) => {
    const accessKey = 'd9b44c21fd7ec3d947d0d627ec036d2ee3784b56f33c9d54a5fedb1ba18e90c0';
    const options = {
        headers: {
            Authorization: `Client-ID ${accessKey}`
        }
    };

    switch (_.toUpper(method)) {
        case 'POST':
            return axios.post(url, data, options);

        case 'GET':
        default:
            return axios.get(url, options);
    }
};

const apiRequestMiddleware = (store) => {
    const { dispatch } = store;

    return (next) => {
        // eslint-disable-next-line
        return async (action) => {
            if (action.type !== API_REQUEST) {
                return next(action);
            }

            const { payload: { PENDING, SUCCESS, FAILURE, query, endpoint, isCollection, method, data } } = action;
            const initialState = getDataInitialState(isCollection);

            let nextState = { ...initialState, isPending: true };
            dispatch({ type: PENDING, payload: nextState });

            try {
                const queryParams = !_.isEmpty(query) ? `?${querystring.stringify(query)}` : '';
                const url = `${API_BASE_URL}/${endpoint}${queryParams}`;
                const response = await handleRequest(url, method, data);
                const statusCode = _.get(response, 'status');

                nextState = { ...initialState, isLoaded: true, data: response.data, statusCode };
                dispatch({ type: SUCCESS, payload: nextState });
            } catch (error) {
                const statusCode = _.get(error, 'response.status');

                nextState = { ...initialState, error: true, statusCode };
                dispatch({ type: FAILURE, payload: nextState });
            }
        };
    };
};

export default apiRequestMiddleware;
