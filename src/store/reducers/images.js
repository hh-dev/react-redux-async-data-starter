import getDataInitialState from '../get-data-initial-state';
import { FETCH_IMAGES } from '../actions/types';

const images = (state = getDataInitialState(true), action) => {
    switch (action.type) {
        case FETCH_IMAGES.SUCCESS:
        case FETCH_IMAGES.PENDING:
        case FETCH_IMAGES.FAILURE:
            return action.payload;

        default:
            return state;
    }
};

export default images;
