import getDataInitialState from '../get-data-initial-state';
import { FETCH_IMAGE } from '../actions/types';

const image = (state = getDataInitialState(), action) => {
    switch (action.type) {
        case FETCH_IMAGE.SUCCESS:
        case FETCH_IMAGE.PENDING:
        case FETCH_IMAGE.FAILURE:
            return action.payload;

        default:
            return state;
    }
};

export default image;
