import { combineReducers } from 'redux';
import image from './image';
import images from './images';

const rootReducer = combineReducers({
    image,
    images
});

export default rootReducer;
