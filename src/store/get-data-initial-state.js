const INITIAL_STATE = { isLoaded: false, isPending: false, error: false, statusCode: 0 };

const getDataInitialState = (isCollection) => {
    const data = isCollection ? [] : {};

    return Object.assign({}, INITIAL_STATE, { data });
};

export default getDataInitialState;
