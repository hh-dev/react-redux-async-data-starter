const asyncActionType = (type) => ({
    PENDING: `${type}_PENDING`,
    SUCCESS: `${type}_SUCCESS`,
    FAILURE: `${type}_FAILURE`
});

export const API_REQUEST = 'API_REQUEST';

export const FETCH_IMAGE = asyncActionType('FETCH_IMAGE');

export const FETCH_IMAGES = asyncActionType('FETCH_IMAGES');
