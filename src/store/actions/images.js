import { API_REQUEST, FETCH_IMAGES } from './types';

const fetchImages = (query) => {
    return {
        type: API_REQUEST,
        payload: {
            ...FETCH_IMAGES,
            isCollection: true,
            endpoint: 'photos',
            query
        }
    };
};

export default {
    fetchImages
};
