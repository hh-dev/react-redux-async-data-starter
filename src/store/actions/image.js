import { API_REQUEST, FETCH_IMAGE } from './types';

const fetchImage = (imageId) => {
    return {
        type: API_REQUEST,
        payload: {
            ...FETCH_IMAGE,
            isCollection: false,
            endpoint: `photos/${imageId}`
        }
    };
};

export default {
    fetchImage
};
