import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Grid, Row, Col } from 'react-bootstrap';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import imageActionCreators from '../../store/actions/image';

class ImagePreview extends Component {
    componentDidMount() {
        const imageId = this.props.match.params.imageId;
        const fetchImage = this.props.actions.fetchImage;

        fetchImage(imageId);
    }

    renderImage(url, id) {
        return (
            <Grid className="component--gallery--image-preview">
                <Row>
                    <Col xs={12}>
                        <Card className="content">
                            <CardHeader title={id} className="header" />
                            <CardMedia
                                image={url}
                                title={id}
                                className="image"
                            />
                        </Card>
                    </Col>
                </Row>
            </Grid>
        );
    }

    render() {
        const { url, id } = this.props.image.data;
        return url && id ? this.renderImage(url, id) : null;
    }
}

const mapStateToProps = (state) => {
    const { image } = state;

    return { image };
};

const mapDispatchToProps = (dispatch) => {
    const actionCreators = Object.assign({}, { ...imageActionCreators });
    const actions = bindActionCreators(actionCreators, dispatch);

    return { actions };
};

ImagePreview.propTypes = {
    match: PropTypes.shape({
        params: PropTypes.shape({
            imageId: PropTypes.string.isRequired
        }).isRequired
    }).isRequired,
    actions: PropTypes.shape({
        fetchImage: PropTypes.func.isRequired
    }).isRequired,
    history: PropTypes.object.isRequired,
    image: PropTypes.shape({
        data: PropTypes.shape({
            id: PropTypes.number,
            url: PropTypes.string
        }).isRequired
    }).isRequired
};

export default connect(mapStateToProps, mapDispatchToProps)(ImagePreview);
