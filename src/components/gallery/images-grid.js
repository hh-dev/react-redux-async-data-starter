import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Grid, Row, Col } from 'react-bootstrap';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import imagesActionCreators from '../../store/actions/images';

class ImagesGrid extends Component {
    componentDidMount() {
        this.props.actions.fetchImages();
    }

    goToImagePreview(id) {
        this.props.history.push(`/images/${id}`);
    }

    renderGridImage(image) {
        const { id, url } = image;

        return (
            <Col xs={12} sm={4} key={id} className="grid-image">
                <Card>
                    <CardHeader
                        title={id}
                        className="header"
                    />
                    <CardMedia
                        image={url}
                        title={id}
                        className="image"
                        onClick={() => this.goToImagePreview(id)}
                    />
                </Card>
            </Col>
        );
    }

    render() {
        const { images } = this.props;

        return (
            <Grid className="component--gallery--images-grid">
                <Row>
                    {images.data.map((image, ix) => this.renderGridImage(image, ix))}
                </Row>
            </Grid>
        );
    }
}

const mapStateToProps = (state) => {
    const { images } = state;

    return { images };
};

const mapDispatchToProps = (dispatch) => {
    const actionCreators = Object.assign({}, { ...imagesActionCreators });
    const actions = bindActionCreators(actionCreators, dispatch);

    return { actions };
};

ImagesGrid.propTypes = {
    actions: PropTypes.shape({
        fetchImages: PropTypes.func.isRequired,
    }).isRequired,
    history: PropTypes.object.isRequired,
    images: PropTypes.shape({
        data: PropTypes.arrayOf(
            PropTypes.shape({
                id: PropTypes.number.isRequired,
                url: PropTypes.string.isRequired
            }).isRequired
        ).isRequired
    }).isRequired
};


export default connect(mapStateToProps, mapDispatchToProps)(ImagesGrid);
