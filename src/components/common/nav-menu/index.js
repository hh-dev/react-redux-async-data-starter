import React, { Component } from 'react';
import { Navbar } from 'react-bootstrap';

class NavMenu extends Component {
    render() {
        return (
            <div className="component--nav-menu">
                <Navbar inverse={true}>
                    <Navbar.Header>
                        <Navbar.Brand>
                            <a href="/">Home</a>
                        </Navbar.Brand>
                    </Navbar.Header>
                </Navbar>
            </div>
        );
    }
}

export default NavMenu;
